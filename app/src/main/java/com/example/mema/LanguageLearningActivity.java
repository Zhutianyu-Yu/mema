package com.example.mema;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LanguageLearningActivity extends AppCompatActivity {
    private Button btn_greeting;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_learning);
    }

    public void showList(View view) {
        btn_greeting = findViewById(R.id.btn0);
        btn_greeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LanguageLearningActivity.this, VocabularyListActivity.class);
                startActivity(intent);
            }
        });
    }
}